var express = require('express');
var router = express.Router();
var wepay = require('../lib/wepay.js').WEPAY;

/* call checkout. */

router.post('/checkout',function(req,res,next){
    // load in your modules
var price  =  req.body.price;
var currency = req.body.currency;
var short_description = req.body.short_description;
var type = req.body.type;

if(!price && !currency && !short_description && !type){
    res.send("Please send all required fields");
    return;
}

var wepay_settings = {
    'access_token'  : 'STAGE_a410e6a9a4805a0ae74aef27ad86c1cd5d08215c52e0ac06e11d463788cb3cd1', // used for oAuth2
    'client_id'     : '55775',
    'client_secret' : '8cfca3d678'
}

var wp = new wepay(wepay_settings);
wp.use_staging(); // use staging environment (payments are not charged)

try {
    wp.call('/checkout/create',
        {
            'account_id': 1276036711,
            'amount': price,
            'currency': currency,
            'short_description': short_description,
            'type': type,
        },
        function(response) {
         res.send(response);
        
        }
    );
} catch (error) {
   rese.send(error);
}
});


module.exports = router;
